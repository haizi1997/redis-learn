package controller;

import entity.Student;
import jdk.nashorn.internal.objects.NativeDate;
import mapper.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import redis.clients.jedis.Jedis;
import com.alibaba.fastjson.JSONArray;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;

public class App {


    @Test
    public  void lianxi2(){
        App app = new App();

        app.testSet("test","test redis dddd");

        final String val = app.testGet("test");

        System.out.println(val);

        app.deleteVal("test");

    }

    @Test
    public void lianxi3() throws Exception {
        App app = new App();
        String val = app.testGet("sno1");
        if(val==null){
            System.out.println("redis不存在该数据");
            Student s = app.findStudentBySno(110);
            System.out.println(s);
            app.setRedis30("sno1",s);

            System.out.println("从数据库查询并存入redis");
        }

    }

        //redis增加数据保存30秒
        public void setRedis30(String key, Student student){
            final Jedis resource = RedisFactory.getInstance().getResource();
            Object obj = JSONArray.toJSON(student);
            String json = obj.toString();
            try {
                resource.set(key,json);
                resource.expire(key,1800);
            }finally {
                resource.close();
            }
        }
        //数据库中查询数据
        public Student findStudentBySno(int sno) throws Exception {
            String resource="SqlMapConfig.xml";

            InputStream inputStream = Resources.getResourceAsStream(resource);

            //创建会话工厂,传入mybatis的配置文件信息
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

            SqlSession sqlSession = sqlSessionFactory.openSession();

            //创建UserMapper的对象
            StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);

            //调用userMapper的方法
            Student student = studentMapper.findStudentBySno(sno);

            sqlSession.close();

            return student;
        }
        //redis增数据
        public  void testSet(String key, String val){
            Jedis resource = RedisFactory.getInstance().getResource();

            try {
                resource.set(key, val);
            }finally{
                resource.close();
            }

            }
       //redis查数据
       public  String testGet(String key){
               Jedis resource = RedisFactory.getInstance().getResource();

            try {
               String val = resource.get(key);
               return val;
            }finally{
                resource.close();
            }


        }
        //redis删除数据
        public void deleteVal(String key){
            final Jedis resource = RedisFactory.getInstance().getResource();
            try {
                resource.del(key);
            }finally {
                resource.close();
            }
        }
}

